WJS
---
Drupal adapter for wjs.
More info about the API on the library pages :
- http://wjs.wexample.com
- https://github.com/weeger/wjs

Install
-------
- Download wjs from https://github.com/weeger/wjs
- Place it into folder : sites/all/libraries/wjs
- Enable the module and have fun.

The module adds custom hooks :

- hook_wjs_register() Allow you to register you extensions
- hook_wjs_startup() Allow you to push extension for "normal" pages load
- hook_wjs_response() Allow you to push extension for "wjs ajax" request only


Copyright
---------
Copyright Romain WEEGER 2010 / 2015
http://www.wexample.com

Licensed under the MIT and GPL licenses :

 - http://www.opensource.org/licenses/mit-license.php
 - http://www.gnu.org/licenses/gpl.html

Thanks
------
I would like to thanks all people who have trusted an encouraged me, consciously
or not. They are not many. Like all of my projects, this program is the result
of a lot of questions, doubts, pain, but joy and love too. I sincerely hope
you'll enjoy it. Thanks to Carole for her incredible Love.
